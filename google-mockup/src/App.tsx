import { useState } from 'react'
import reactLogo from './assets/react.svg'
import googleLogo from './assets/googlelogo.png'
import headerStyles from './Header.module.css';
import footerStyles from './Footer.module.css';
import './App.css'

function Header() {
  return (
    <header className={headerStyles.header}>
      <nav className={headerStyles.header_left_nav}>
        <ul>
          <li><a href='#'>About</a></li>
          <li><a href='#'>Store</a></li>
        </ul>
      </nav>
      <nav className={headerStyles.header_right_nav}>
        <ul>
          <li><a href='#'>Gmail</a></li>
          <li><a href='#'>Images</a></li>
          <li><img src={reactLogo} alt='React Logo' /></li>
          <button>Sign in</button>
        </ul>
      </nav>
    </header>
  )
}

function Footer() {
  return (
    <footer className={footerStyles.footer}>
      <div className={footerStyles.footer_content}>
        <div className={footerStyles.footer_left}>
          <span>&copy; {new Date().getFullYear()} Google</span>
        </div>
        <div className={footerStyles.footer_right}>
          <a href="#">Privacy</a>
          <a href="#">Terms</a>
          <a href="#">Settings</a>
        </div>
      </div>
    </footer>
  );
}

function App() {

  return (
    <div className='App'>
      <Header />
      <div className='logo'>
        <img src={googleLogo} alt='Google Logo' />
      </div>
      <div className='searchBar'>
        <input type='text' placeholder='Search Google or type a url' />
      </div>
      <div className='searchButton'>
        <button>Google Search</button>
        <button>I'm Feeling Lucky</button>
      </div>
      <Footer />
    </div>
  )
}

export default App
