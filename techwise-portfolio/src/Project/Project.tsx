import React from "react";
import styles from "./Project.module.css";
import { Button, CardMedia } from "@mui/material";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import { projectProps } from "../common/types";

// Define Project component
const Project: React.FC<
  projectProps & { changeTheme: (theme: string) => void }
> = ({
  title,
  color,
  date,
  description,
  imageUrl,
  url,
  skills,
  isFocused,
  onClick,
  changeTheme,
}) => {
  const handleThemeChange = () => {
    changeTheme(color);
  };

  return (
    <div className={styles.project}>
      <Card
        sx={{ maxWidth: 345 }}
        className={`project ${isFocused ? "focused" : ""}`}
        onClick={onClick}
      >
        <CardContent>
          <CardMedia
            component="img"
            height="140"
            image={imageUrl}
            alt={title}
          />
          <Typography variant="h5" component="div">
            {title}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {date}
          </Typography>
        </CardContent>
        {isFocused && (
          <div className="projectDetails">
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {description}
              </Typography>
            </CardContent>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                {skills.join(", ")}
              </Typography>
            </CardContent>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                <a href={url} target="_blank" rel="noopener noreferrer">
                  View Project
                </a>
              </Typography>
            </CardContent>
            <CardContent>
              <Typography variant="body2" color="text.secondary">
                <Button variant="contained" onClick={handleThemeChange}>
                  Change Theme
                </Button>
              </Typography>
            </CardContent>
          </div>
        )}
      </Card>
    </div>
  );
};

export default Project;
