// Define project type
type projectProps = {
  id: number;
  title: string;
  color: string;
  date: string;
  description: string;
  imageUrl: string;
  url: string;
  skills: string[];
  isFocused?: boolean;
  onClick?: () => void;
};

// Export project type
export type { projectProps };
