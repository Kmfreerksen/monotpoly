import React from 'react';
import './Footer.module.css';

const Footer: React.FC = () => {
  return (
    <footer>
      <p>&copy; 2024 K. Freerksen</p>
    </footer>
  );
}

export default Footer;