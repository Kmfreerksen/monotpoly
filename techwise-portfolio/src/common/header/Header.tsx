import React from "react";
import styles from "./Header.module.css"; // Import the CSS module

const Header: React.FC<{ themeColor: string }> = ({ themeColor }) => {
  return (
    <header className={styles.header} style={{ backgroundColor: themeColor }}>
      <h1>Katie Freerksen</h1>
      <h3>Software Engineer, ITS Systems Analyst</h3>
    </header>
  );
};

export default Header;
