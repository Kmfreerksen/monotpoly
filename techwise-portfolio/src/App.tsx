import { useState } from "react";
import React from "react";
import "./App.global.css";
import styles from "./App.module.css";
import Header from "./common/header/Header";
import Footer from "./common/footer/Footer";
import Project from "./Project/Project";
import { projectProps } from "./common/types";
import Card from "@mui/material/Card";
import Typography from "@mui/material/Typography";
import { CardHeader } from "@mui/material";

const projectsData: projectProps[] = [
  {
    id: 1,
    title: "Project 1",
    color: "red",
    date: "January 2023",
    description: "Description of Project 1",
    imageUrl: "https://picsum.photos/id/1011/300/200",
    url: "https://www.google.com",
    skills: ["HTML", "CSS", "JavaScript"],
  },
  {
    id: 2,
    title: "Project 2",
    color: "blue",
    date: "March 2024",
    description: "Description of Project 2",
    imageUrl: "https://picsum.photos/id/1022/300/200",
    url: "https://www.google.com",
    skills: ["React", "TypeScript", "Node.js"],
  },
  {
    id: 3,
    title: "Project 3",
    color: "green",
    date: "August 2024",
    description: "Description of Project 3",
    imageUrl: "https://picsum.photos/id/1033/300/200",
    url: "https://www.google.com",
    skills: ["BootstrapVue", "JavaScript", "Node.js"],
  },
  // Add more projects as needed
];

// Define App component
const App: React.FC = () => {
  // Define state to keep track of the current theme, allowing for multiple themes
  const [theme, setTheme] = useState<string>("light"); // Default theme is light

  // Define function to change theme to a color, light or dark based on the string passed in
  const changeTheme = (theme: string) => {
    setTheme(theme);
  };

  // Define state to keep track of the project that is currently focused
  const [focusedProjectId, setFocusedProjectId] = useState<number | null>(null); // null means no project is focused

  // Define function to handle project click
  const handleProjectClick = (projectId: number) => {
    setFocusedProjectId(projectId === focusedProjectId ? null : projectId); // If the clicked project is already focused, unfocus it
  };

  // Render App component
  return (
    <div className={`${styles.app} ${styles[theme]}`}>
      <Header themeColor={theme} />
      <Card className={styles.projectCategoryContainer}>
        <CardHeader
          title={
            <Typography variant="h4" component="div">
              Full Stack Projects
            </Typography>
          }
        />
        <div className={styles.projectContainer}>
          {projectsData.map((project) => (
            <Project
              key={project.id}
              {...project}
              isFocused={focusedProjectId === project.id}
              onClick={() => handleProjectClick(project.id)}
              changeTheme={changeTheme} // Pass the changeTheme function to the Project component
            />
          ))}
        </div>
      </Card>
      <Footer />
    </div>
  );
};

export default App;
